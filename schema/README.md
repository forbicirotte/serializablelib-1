Schemas created using [Plant UML](http://plantuml.com/)

In order to modify it you can:

- use the `PlantUML integration` JetBrains Plugin in Android Studio 

- go to Plant UML site and copy/paste the .puml file in the example box