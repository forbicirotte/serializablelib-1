package com.life.serializable.stream;

import android.support.annotation.NonNull;
import android.util.Log;

import com.life.serializable.bleClient.SerializaBLEClient;
import com.life.serializable.bleServer.SerializaBLEServer;
import com.life.serializable.interfaces.SerializaBLEOutputListener;
import com.life.serializable.utils.Consts.Role;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.life.serializable.utils.Consts.MTU_MINIMUM_SIZE;

/**
 * Created by User on 12/02/2018.
 */

public class SerializaBLEOutputStream extends OutputStream implements SerializaBLEOutputListener {

    private BlockingQueue<Byte> writeBuffer;
    private final String TAG = "SerializaBLEOS";
    private Role role;
    private int maxTransfertSize = MTU_MINIMUM_SIZE - 3;
    private AtomicBoolean isWriting, isReading;
    private Timer timeout;
    public static int WRITE_TIMEOUT = 1000;

    public SerializaBLEOutputStream(Role role) {
        this.role = role;
        writeBuffer = new LinkedBlockingQueue<>();
        isWriting = new AtomicBoolean(false);
        isReading = new AtomicBoolean(true);
    }

    @Override
    public void write(int b) throws IOException {
        writeBuffer.offer((byte) b);
    }

    @Override
    public void write(@NonNull byte[] b) throws IOException {
        Log.d("outputStream", "write() called with: b = [" + b.length + "]");

        for (byte d : b) {
            write(d);
        }

        if (!isWriting.get() && !isReading.get())
            onDataSent();

    }


    @Override
    public void flush() throws IOException {
//        onDataSent();
    }

    @Override
    public void onDataSent() throws IOException {
        Log.d(TAG, "onDataSent() called");

        if (isReading.get())
            return;

        cancelTimeout();

        if (writeBuffer.isEmpty()) {
            isWriting.set(false);
            return;
        } else
            isWriting.set(true);

        Log.d(TAG, "onDataSent: writing...");

        createWriteTimeout();

        ByteBuffer chunck;
        int chunckLength;
        if (writeBuffer.size() < maxTransfertSize) {
            chunckLength = writeBuffer.size();
        } else
            chunckLength = maxTransfertSize;

        chunck = ByteBuffer.allocate(chunckLength);

        for (int i = 0; i < chunckLength; i++) {
            try {
                chunck.put(writeBuffer.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        byte[] chunkArray = chunck.array();

        try {
            switch (role) {
                case CLIENT:
                    SerializaBLEClient.getInstance().writeCharacteristic(chunkArray);
                    break;
                case SERVER:
                    SerializaBLEServer.getInstance().writeCharacteristic(chunkArray);
                    break;
            }
        } catch (IOException e) {
            writeBuffer.clear();
            throw e;
        }

    }

    @Override
    public void onMaxTrasfertSizeChanged(int size) {
        maxTransfertSize = size;
    }

    @Override
    public void onReadingStateChanged(boolean b) {
        isReading.set(b);

        if (!isReading.get()) {
            try {
                onDataSent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createWriteTimeout() {
        if (timeout == null) {
            timeout = new Timer();
            timeout.schedule(new TimerTask() {
                @Override
                public void run() {
                    Log.d(TAG, "Write Timeout");
                    writeBuffer.clear();
                    isWriting.set(false);
                }
            }, WRITE_TIMEOUT);

        }
    }

    private void cancelTimeout() {
        if (timeout != null) {
            timeout.cancel();
            timeout = null;
        }
    }
}
