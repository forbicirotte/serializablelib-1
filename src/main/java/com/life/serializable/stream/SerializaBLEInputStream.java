package com.life.serializable.stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.life.serializable.interfaces.SerializaBLEInputListener;
import com.life.serializable.utils.Consts.Role;

/**
 * Created by User on 12/02/2018.
 */

public class SerializaBLEInputStream extends InputStream implements SerializaBLEInputListener {

    BlockingQueue<Integer> readBuffer;
    Role role;
    boolean EOSsent = false;

    public SerializaBLEInputStream(Role role) {
        this.role = role;
        readBuffer = new LinkedBlockingQueue<>();
    }

    @Override
    public int read() throws IOException {
        try {

            if (readBuffer.isEmpty() && !EOSsent) {
                EOSsent = true;
                return -1;
            }

            EOSsent = false;

            return readBuffer.take();
        } catch (InterruptedException e) {
            throw new IOException("SerializaBLEInputStream Exception " + e.getMessage());
        }
    }

    @Override
    public int available() throws IOException {
        return readBuffer.size();
    }

    @Override
    public void onDataReceived(byte[] data) {
        for (byte b : data) {
            readBuffer.offer(b&0xFF);
        }
    }

}

