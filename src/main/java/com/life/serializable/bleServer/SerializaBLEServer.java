package com.life.serializable.bleServer;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.util.Log;

import com.life.serializable.interfaces.SerializaBLEInputListener;
import com.life.serializable.interfaces.SerializaBLEListener;
import com.life.serializable.interfaces.SerializaBLEOutputListener;
import com.life.serializable.utils.Consts;
import com.life.serializable.utils.Utils;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static android.bluetooth.BluetoothGatt.GATT_FAILURE;
import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static com.life.serializable.utils.Consts.ACK;
import static com.life.serializable.utils.Consts.CHARACTERISTIC_RX_UUID;
import static com.life.serializable.utils.Consts.CHARACTERISTIC_TX_UUID;
import static com.life.serializable.utils.Consts.DESCRIPTOR_CONFIG_UUID;
import static com.life.serializable.utils.Consts.MTU_MINIMUM_SIZE;
import static com.life.serializable.utils.Consts.READ_TIMEOUT;
import static com.life.serializable.utils.Consts.SERVICE_UUID;
import static com.life.serializable.utils.Consts.getGattStatusString;

/**
 * Created by User on 01/02/2018.
 */

public class SerializaBLEServer extends BluetoothGattServerCallback {

    private final static String TAG = "SerializaBLEServer";

    private static SerializaBLEServer instance = new SerializaBLEServer();

    public static SerializaBLEServer getInstance() {
        return instance;
    }

    private SerializaBLEListener serializaBLEListener;

    private SerializaBLEOutputListener serializaBLEOutputListener;
    private SerializaBLEInputListener serializaBLEInputListener;

    private BluetoothGattServer mGattServer;
    private BluetoothDevice bluetoothDevice;

    private BluetoothGattService service;
    private BluetoothGattCharacteristic chTX;
    private BluetoothGattDescriptor chTXNotifyDescr;
    private BluetoothGattCharacteristic chRX;
    private BluetoothGattDescriptor chRXNotifyDescr;

    private SerializaBLEServer() {

        service = new BluetoothGattService(SERVICE_UUID, BluetoothGattService.SERVICE_TYPE_PRIMARY);
        chTX = new BluetoothGattCharacteristic(CHARACTERISTIC_TX_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE |
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_WRITE
        );
        chRX = new BluetoothGattCharacteristic(CHARACTERISTIC_RX_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE |
                        BluetoothGattCharacteristic.PROPERTY_READ |
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_WRITE |
                        BluetoothGattCharacteristic.PERMISSION_READ
        );

        //Needed by iOS device for notifications
        chTXNotifyDescr = new BluetoothGattDescriptor(DESCRIPTOR_CONFIG_UUID, BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
        chTX.addDescriptor(chTXNotifyDescr);
        chRXNotifyDescr = new BluetoothGattDescriptor(DESCRIPTOR_CONFIG_UUID, BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
        chRX.addDescriptor(chRXNotifyDescr);

        service.addCharacteristic(chRX);
        service.addCharacteristic(chTX);
    }

    public void setSerializaBLEListener(SerializaBLEListener serializaBLEListener) {
        this.serializaBLEListener = serializaBLEListener;
    }

    public void setSerializaBLEOutputListener(SerializaBLEOutputListener serializaBLEOutputListener) {
        this.serializaBLEOutputListener = serializaBLEOutputListener;
        setReadingState(false);
    }

    public void setSerializaBLEInputListener(SerializaBLEInputListener serializaBLEInputListener) {
        this.serializaBLEInputListener = serializaBLEInputListener;
    }

    public void setmGattServer(BluetoothGattServer mGattServer) {
        this.mGattServer = mGattServer;
    }

    public BluetoothGattService getService() {
        return service;
    }

    /**
     * (SDK)
     * A remote client has requested to read a local characteristic.
     * <p>
     * <p>An application must call {@link BluetoothGattServer#sendResponse}
     * to complete the request.
     *
     * @param device         The remote device that has requested the read operation
     * @param requestId      The Id of the request
     * @param offset         Offset into the value of the characteristic
     * @param characteristic Characteristic to be read
     */
    @Override
    public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
        super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
        Log.d(TAG, "onCharacteristicReadRequest() called with: device = [" + device.getAddress() + "], requestId = [" + requestId + "], offset = [" + offset + "], characteristic = [" + characteristic.getUuid().toString() + "]");

        if (characteristic.equals(chRX)) {
            mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, chRX.getValue());
            return;
        }

        mGattServer.sendResponse(device, requestId, GATT_FAILURE, 0, null);
    }

    /**
     * (SDK)
     * A remote client has requested to write to a local characteristic.
     * <p>
     * <p>An application must call {@link BluetoothGattServer#sendResponse}
     * to complete the request.
     *
     * @param device         The remote device that has requested the write operation
     * @param requestId      The Id of the request
     * @param characteristic Characteristic to be written to.
     * @param preparedWrite  true, if this write operation should be queued for
     *                       later execution.
     * @param responseNeeded true, if the remote device requires a response
     * @param offset         The offset given for the value
     * @param value          The value the client wants to assign to the characteristic
     */
    @Override
    public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
        super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value);

        Log.d(TAG, "onCharacteristicWriteRequest() called with: device = [" + device.getAddress() + "], requestId = [" + requestId + "], characteristic = [" + characteristic.getUuid().toString() + "], preparedWrite = [" + preparedWrite + "], responseNeeded = [" + responseNeeded + "], offset = [" + offset + "]");

        if (characteristic.equals(chTX)) {

            Log.w(TAG, "Start C->S transaction: " + System.currentTimeMillis());

            cancelTimeout();
            setReadingState(true);

            if (serializaBLEInputListener != null)
                serializaBLEInputListener.onDataReceived(value);

            if (responseNeeded)
                mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, null);

            chTX.setValue(new byte[]{Consts.ACK});

            if (bluetoothDevice != null)
                mGattServer.notifyCharacteristicChanged(device, chTX, true);

//            cancelTimeout();

//            setReadingState(false);

            return;
        }

        if (characteristic.equals(chRX)) {

            byte res = value[0];

            if (res == ACK) {
                if (responseNeeded)
                    mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, null);

                Log.w(TAG, "Stop S->C transaction: " + System.currentTimeMillis());

                if (serializaBLEOutputListener != null)
                    try {
                        serializaBLEOutputListener.onDataSent();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            return;
        }

        if (responseNeeded)
            mGattServer.sendResponse(device, requestId, GATT_FAILURE, 0, null);
    }

    /**
     * (SDK)
     * Callback indicating when a remote device has been connected or disconnected.
     *
     * @param device   Remote device that has been connected or disconnected.
     * @param status   Status of the connect or disconnect operation.
     * @param newState Returns the new connection state. Can be one of
     *                 {@link BluetoothProfile#STATE_DISCONNECTED} or
     *                 {@link BluetoothProfile#STATE_CONNECTED}
     */
    @Override
    public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
        super.onConnectionStateChange(device, status, newState);

        if (serializaBLEListener != null)
            serializaBLEListener.onConnectionStateChanged(device, newState);

        String newStateStr = "UNKNOWN";

        switch (newState) {
            case BluetoothProfile.STATE_CONNECTED:

                if (bluetoothDevice == null) {
                    this.bluetoothDevice = device;
//                stopAdvertising();
                    newStateStr = "STATE_CONNECTED";

                    if (serializaBLEListener != null)
                        serializaBLEListener.onChannelReady();
                } else {
                    mGattServer.cancelConnection(device);
                }

                break;
            case BluetoothProfile.STATE_DISCONNECTED:

                newStateStr = "STATE_DISCONNECTED";

                if (bluetoothDevice != null && bluetoothDevice.equals(device)) {
                    this.bluetoothDevice = null;

                    if (serializaBLEListener != null)
                        serializaBLEListener.onChannelLost();
                }
//                startAdvertising();

                break;
        }

        Log.d(TAG, "onConnectionStateChange() called with: device = [" + device.getName() + " " + device.getAddress() + "], status = [" + getGattStatusString(status) + "], newState = [" + newStateStr + "]");
    }

    /**
     * (SDK)
     * A remote client has requested to read a local descriptor.
     * <p>
     * <p>An application must call {@link BluetoothGattServer#sendResponse}
     * to complete the request.
     *
     * @param device     The remote device that has requested the read operation
     * @param requestId  The Id of the request
     * @param offset     Offset into the value of the characteristic
     * @param descriptor Descriptor to be read
     */
    @Override
    public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
        super.onDescriptorReadRequest(device, requestId, offset, descriptor);
        Log.d(TAG, "onDescriptorReadRequest() called with: device = [" + device.getAddress() + "], requestId = [" + requestId + "], offset = [" + offset + "], descriptor = [" + descriptor.getUuid().toString() + "]");
    }

    /**
     * (SDK)
     * A remote client has requested to write to a local descriptor.
     * <p>
     * <p>An application must call {@link BluetoothGattServer#sendResponse}
     * to complete the request.
     *
     * @param device         The remote device that has requested the write operation
     * @param requestId      The Id of the request
     * @param descriptor     Descriptor to be written to.
     * @param preparedWrite  true, if this write operation should be queued for
     *                       later execution.
     * @param responseNeeded true, if the remote device requires a response
     * @param offset         The offset given for the value
     * @param value          The value the client wants to assign to the descriptor
     */
    @Override
    public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
        super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
        Log.d(TAG, "onDescriptorWriteRequest() called with: device = [" + device + "], requestId = [" + requestId + "], descriptor = [" + descriptor + "], preparedWrite = [" + preparedWrite + "], responseNeeded = [" + responseNeeded + "], offset = [" + offset + "], value = [" + Utils.bytesToHex(value, value.length) + "]");

        if (DESCRIPTOR_CONFIG_UUID.equals(descriptor.getUuid())) {
            if (responseNeeded) {
                mGattServer.sendResponse(device, requestId, GATT_SUCCESS, 0, null);
            }
        }

    }

    /**
     * (SDK)
     * Execute all pending write operations for this device.
     * <p>
     * <p>An application must call {@link BluetoothGattServer#sendResponse}
     * to complete the request.
     *
     * @param device    The remote device that has requested the write operations
     * @param requestId The Id of the request
     * @param execute   Whether the pending writes should be executed (true) or
     *                  cancelled (false)
     */
    @Override
    public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
        super.onExecuteWrite(device, requestId, execute);
        Log.d(TAG, "onExecuteWrite() called with: device = [" + device.getAddress() + "], requestId = [" + requestId + "], execute = [" + execute + "]");
    }

    /**
     * (SDK)
     * Callback indicating the MTU for a given device connection has changed.
     * <p>
     * <p>This callback will be invoked if a remote client has requested to change
     * the MTU for a given connection.
     *
     * @param device The remote device that requested the MTU change
     * @param mtu    The new MTU size
     */
    @Override
    public void onMtuChanged(BluetoothDevice device, int mtu) {
        super.onMtuChanged(device, mtu);

        Log.d(TAG, "onMtuChanged() called with: device = [" + device.getName() + "], mtu = [" + mtu + "]");

        // Receive the requested MTU size.
        // See also https://stackoverflow.com/questions/24135682/android-sending-data-20-bytes-by-ble
        // It discounts 3 bytes of metadata.

        if (serializaBLEOutputListener != null)
            serializaBLEOutputListener.onMaxTrasfertSizeChanged(mtu - 3);

    }

    /**
     * (SDK)
     * Callback invoked when a notification or indication has been sent to
     * a remote device.
     * <p>
     * <p>When multiple notifications are to be sent, an application must
     * wait for this callback to be received before sending additional
     * notifications.
     *
     * @param device The remote device the notification has been sent to
     * @param status {@link BluetoothGatt#GATT_SUCCESS} if the operation was successful
     */
    @Override
    public void onNotificationSent(BluetoothDevice device, int status) {
        super.onNotificationSent(device, status);
        Log.d(TAG, "onNotificationSent() called with: device = [" + device.getAddress() + "], status = [" + getGattStatusString(status) + "]");

        if (isReading) {
            Log.w(TAG, "Stop C->S transaction: " + System.currentTimeMillis());

            createReadTimeout();
        }

    }

    /**
     * (SDK)
     * Indicates whether a local service has been added successfully.
     *
     * @param status  Returns {@link BluetoothGatt#GATT_SUCCESS} if the service
     *                was added successfully.
     * @param service The service that has been added
     */
    @Override
    public void onServiceAdded(int status, BluetoothGattService service) {
        super.onServiceAdded(status, service);

        Log.d(TAG, "onServiceAdded() called with: status = [" + getGattStatusString(status) + "], service = [" + service.getUuid().toString() + "]");
    }


    /**
     * Writes next packet to {@link SerializaBLEServer#chRX} and notifies the Client (with indication) {@link BluetoothGattServer#notifyCharacteristicChanged(BluetoothDevice, BluetoothGattCharacteristic, boolean)}
     */
    public void writeCharacteristic(byte[] data) throws IOException {

        Log.d(TAG, "writeCharacteristic: " + data.length);

        Log.w(TAG, "Start S->C transaction: " + System.currentTimeMillis());

        if (bluetoothDevice != null) {
            boolean setValue = chRX.setValue(data);

            boolean notify = mGattServer.notifyCharacteristicChanged(bluetoothDevice, chRX, true);

            if (!(setValue && notify))
                throw new IOException("Error during write");

        } else
            throw new IOException("No devices!");
    }

    void clearAndInit() {
        if (serializaBLEOutputListener != null)
            serializaBLEOutputListener.onMaxTrasfertSizeChanged(MTU_MINIMUM_SIZE - 3);

        this.bluetoothDevice = null;

        if (serializaBLEListener != null)
            serializaBLEListener.onChannelLost();

        setReadingState(false);
    }

    boolean isReading;

    private void setReadingState(boolean b) {
        isReading = b;
        if (serializaBLEOutputListener != null)
            serializaBLEOutputListener.onReadingStateChanged(b);
    }

    private Timer timeout;

    private void createReadTimeout() {
        cancelTimeout();

        if (timeout == null) {
            timeout = new Timer();
            timeout.schedule(new TimerTask() {
                @Override
                public void run() {
                    Log.d(TAG, "Read Timeout");
                    setReadingState(false);
                }
            }, READ_TIMEOUT);

        }
    }

    private void cancelTimeout() {
        if (timeout != null) {
            timeout.cancel();
            timeout = null;
        }
    }
}
