/*
* MIT License
*
* Copyright (c) 2015 Douglas Nassif Roma Junior
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

package com.life.serializable.bleServer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.ParcelUuid;
import android.util.Log;

import com.life.serializable.stream.SerializaBLEInputStream;
import com.life.serializable.stream.SerializaBLEOutputStream;
import com.life.serializable.utils.Consts.Role;

import static com.life.serializable.utils.Consts.SERVICE_UUID;

/**
 * Created by User on 22/01/2018.
 */

public class BLEServerManager {

    private final static String TAG = "BLEServerManager";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGattServer mGattServer;

    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;

    SerializaBLEOutputStream serializaBLEOutputStream;
    SerializaBLEInputStream serializaBLEInputStream;

    private Context context;

    public BLEServerManager(Context context) {

        this.context = context;

        serializaBLEOutputStream = new SerializaBLEOutputStream(Role.SERVER);
        serializaBLEInputStream = new SerializaBLEInputStream(Role.SERVER);

        mBluetoothManager = (BluetoothManager) this.context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        Log.d(TAG, "BLEServerManager: this is my address " + mBluetoothAdapter.getAddress());

        if (!mBluetoothAdapter.isEnabled())
            enableBt();
        else
            startBle();

        context.registerReceiver(STATE_CHANGED, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        context.registerReceiver(SCAN_MODE_CHANGED, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));

        context.registerReceiver(PAIRING_REQUEST, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST));

        context.registerReceiver(BOND_STATE_CHANGED, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
    }

    BroadcastReceiver STATE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)) {
                case BluetoothAdapter.STATE_ON:
                    Log.d(TAG, "State Changed: STATE_ON");
//                        mBluetoothAdapter.setScanMode(BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE,0);
                    startBle();
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d(TAG, "State Changed: STATE_TURNING_ON");
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Log.d(TAG, "State Changed: STATE_TURNING_OFF");
                    SerializaBLEServer.getInstance().clearAndInit();
//                    stopBle();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    Log.d(TAG, "State Changed: STATE_OFF");
//                    stopAdvertising();
                    break;
            }
        }
    };

    BroadcastReceiver SCAN_MODE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Scan Changed");
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.SCAN_MODE_NONE)) {
                case BluetoothAdapter.SCAN_MODE_NONE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_NONE");
                    break;
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_CONNECTABLE");
                    break;
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_CONNECTABLE_DISCOVERABLE");
                    break;
            }

        }
    };

    BroadcastReceiver PAIRING_REQUEST = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            int pairingVariant = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT, BluetoothDevice.ERROR);
            int pairingKey = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_KEY, BluetoothDevice.ERROR);

            Log.d(TAG, "ACTION_PAIRING_REQUEST - " + dev.getName() + " " + dev.getAddress() + "\n" + pairingVariant + " " + pairingKey);

            String pairVar = "UNKNOWN";

            switch (pairingVariant) {
                case BluetoothDevice.PAIRING_VARIANT_PIN:
                    pairVar = "PAIRING_VARIANT_PIN";
                    break;
                    /*case BluetoothDevice.PAIRING_VARIANT_PASSKEY:
                        pairVar = "PAIRING_VARIANT_PASSKEY";
                        break;*/
                case BluetoothDevice.PAIRING_VARIANT_PASSKEY_CONFIRMATION:
                    pairVar = "PAIRING_VARIANT_PASSKEY_CONFIRMATION";
                    break;
                    /*case BluetoothDevice.PAIRING_VARIANT_CONSENT:
                        pairVar = "PAIRING_VARIANT_CONSENT";
                        break;
                    case BluetoothDevice.PAIRING_VARIANT_DISPLAY_PASSKEY:
                        pairVar = "PAIRING_VARIANT_DISPLAY_PASSKEY";
                        break;
                    case BluetoothDevice.PAIRING_VARIANT_DISPLAY_PIN:
                        pairVar = "PAIRING_VARIANT_DISPLAY_PIN";
                        break;
                    case BluetoothDevice.PAIRING_VARIANT_OOB_CONSENT:
                        pairVar = "PAIRING_VARIANT_OOB_CONSENT";
                        break;*/
            }

            Log.d(TAG, pairVar);

            //TODO: Handle pairing in a more secure way
            //Since we don't have any UI just confirm any request
               /* if (pairingKey == BluetoothDevice.ERROR) {
                    try {

                        pairingKey=0;
                        byte[] pinBytes;
                        pinBytes = (""+pairingKey).getBytes("UTF-8");
                        dev.setPin(pinBytes);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                boolean pairConf = dev.setPairingConfirmation(true);

                Log.d(TAG, pairVar + " - setPairingconfirmation: "
                        + pairConf
                );*/


        }
    };

    BroadcastReceiver BOND_STATE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            String bond = "BOND_NONE";

            switch (intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE)) {
                case BluetoothDevice.BOND_NONE:
                    bond = "BOND_NONE";
                    break;
                case BluetoothDevice.BOND_BONDING:
                    bond = "BOND_BONDING";
                    break;
                case BluetoothDevice.BOND_BONDED:
                    bond = "BOND_BONDED";
                    break;
            }

            Log.d(TAG, "ACTION_BOND_STATE_CHANGED - " + bond + " " + dev.getName() + " " + dev.getAddress());
        }
    };

    public void enableBt() {
        if (mBluetoothAdapter == null)
            return;

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    public void disableBt() {
        if (mBluetoothAdapter == null)
            return;

        if (mBluetoothAdapter.isEnabled()) {

            stopBle();

            mBluetoothAdapter.disable();
        }
    }

    public void createBond(BluetoothDevice bluetoothDevice) {
//
//        try {
//            String pin = "270586";
//
//            bluetoothDevice.setPin(pin.getBytes("UTF-8"));

        boolean bond = bluetoothDevice.createBond();

        Log.d(TAG, "createBond: " + bond);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
    }

    private void startBle() {
        initAdvertiser();
        initGattServer();

        startAdvertising();
    }

    public void stopBle() {
        stopAdvertising();
        stopServer();
    }

    /**
     * Verifies HW compatibility and instatiates LeAdvertiser
     */
    private void initAdvertiser() {

        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.e(TAG, "This device doesn't supports BLE");
            return;
        }

        if (!mBluetoothAdapter.isMultipleAdvertisementSupported()) {
            Log.e(TAG, "This device doesn't supports MultipleAdvertisement");
            return;
        }

        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
    }

    /**
     * Sets and starts LeAdvertising
     */
    public void startAdvertising() {
        Log.d(TAG, "startAdvertising() called");

        if (mBluetoothLeAdvertiser == null) {
            return;
        }
        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                // AdvertiseMode refers to how frequently the server will send out an advertising packet. Higher frequency will consume more energy, and low frequency will use less.
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                // TxPowerLevel deals with the broadcast range, so a higher level will have a larger area in which devices will be able to find it.
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                .build();

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .build();

        ParcelUuid parcelUuid = new ParcelUuid(SERVICE_UUID);

        AdvertiseData scanResp = new AdvertiseData.Builder()
                .addServiceUuid(parcelUuid)
                .build();

        mBluetoothLeAdvertiser.startAdvertising(settings, data, scanResp, mAdvertiseCallback);
    }

    /**
     * Bluetooth LE advertising callbacks, used to deliver advertising operation status.
     */
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            Log.d(TAG, "Peripheral advertising started.");
        }

        @Override
        public void onStartFailure(int errorCode) {

            String error = "UNKNOWN";

            switch (errorCode) {
                case ADVERTISE_FAILED_DATA_TOO_LARGE:
                    error = "ADVERTISE_FAILED_DATA_TOO_LARGE";
                    break;
                case ADVERTISE_FAILED_ALREADY_STARTED:
                    error = "ADVERTISE_FAILED_ALREADY_STARTED";
                    break;
                case ADVERTISE_FAILED_INTERNAL_ERROR:
                    error = "ADVERTISE_FAILED_INTERNAL_ERROR";
                    break;
            }

            Log.d(TAG, "Peripheral advertising failed: " + error);
        }
    };


    /**
     * Open a GATT server with {@link SerializaBLEServer#service} containing {@link SerializaBLEServer#chTX}
     * and {@link SerializaBLEServer#chRX}
     * <p>
     * NB: RX and TX are defined in a Client view
     */
    public void initGattServer() {

        mGattServer = mBluetoothManager.openGattServer(context, SerializaBLEServer.getInstance());

        if (mGattServer != null) {
            SerializaBLEServer.getInstance().setmGattServer(mGattServer);
            SerializaBLEServer.getInstance().setSerializaBLEInputListener(serializaBLEInputStream);
            SerializaBLEServer.getInstance().setSerializaBLEOutputListener(serializaBLEOutputStream);

            mGattServer.addService(SerializaBLEServer.getInstance().getService());
        } else {
            Log.e(TAG, "initGattServer: GATT server is null!!!");
        }

    }

    /**
     * Close current GATT server
     */
    private void stopServer() {
        if (mGattServer != null) {
            mGattServer.close();
        }
    }

    /**
     * Stops LeAdvertising
     */
    public void stopAdvertising() {
        Log.d(TAG, "stopAdvertising() called");
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
        }
    }

    public void unregisterReceivers() {
        context.unregisterReceiver(STATE_CHANGED);
        context.unregisterReceiver(PAIRING_REQUEST);
        context.unregisterReceiver(BOND_STATE_CHANGED);
        context.unregisterReceiver(SCAN_MODE_CHANGED);
    }

    public SerializaBLEInputStream getSerializaBLEInputStream() {
        return serializaBLEInputStream;
    }

    public SerializaBLEOutputStream getSerializaBLEOutputStream() {
        return serializaBLEOutputStream;
    }
}


