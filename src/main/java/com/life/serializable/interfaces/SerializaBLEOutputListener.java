package com.life.serializable.interfaces;

import java.io.IOException;

/**
 * Created by User on 13/02/2018.
 */

public interface SerializaBLEOutputListener {

    void onDataSent() throws IOException;

    void onMaxTrasfertSizeChanged(int size);

    void onReadingStateChanged(boolean b);
}
