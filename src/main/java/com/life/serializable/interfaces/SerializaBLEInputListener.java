package com.life.serializable.interfaces;

/**
 * Created by User on 13/02/2018.
 */

public interface SerializaBLEInputListener {

    void onDataReceived(byte[] data);

}
