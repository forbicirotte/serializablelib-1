package com.life.serializable.interfaces;

import android.bluetooth.BluetoothDevice;

/**
 * Created by User on 02/02/2018.
 */

public interface SerializaBLEListener {
    void onConnectionStateChanged(BluetoothDevice bluetoothDevice, int status);

    void onChannelReady();

    void onChannelLost();
}
