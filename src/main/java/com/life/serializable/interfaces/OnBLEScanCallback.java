package com.life.serializable.interfaces;

import android.bluetooth.BluetoothDevice;

/**
 * Created by User on 02/02/2018.
 */

public interface OnBLEScanCallback {
    void onDeviceDiscovered(BluetoothDevice device, int rssi);

    void onStartScan();

    void onStopScan();
}
