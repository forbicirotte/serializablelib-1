package com.life.serializable.bleClient;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.life.serializable.interfaces.SerializaBLEInputListener;
import com.life.serializable.interfaces.SerializaBLEListener;
import com.life.serializable.interfaces.SerializaBLEOutputListener;

import static com.life.serializable.utils.Consts.ACK;
import static com.life.serializable.utils.Consts.CHARACTERISTIC_RX_UUID;
import static com.life.serializable.utils.Consts.CHARACTERISTIC_TX_UUID;
import static com.life.serializable.utils.Consts.READ_TIMEOUT;
import static com.life.serializable.utils.Consts.SERVICE_UUID;

/**
 * Created by User on 02/02/2018.
 */

public class SerializaBLEClient extends BluetoothGattCallback {

    private final static String TAG = "SerializaBLEClient";

    private static SerializaBLEClient instance = new SerializaBLEClient();

    public static SerializaBLEClient getInstance() {
        return instance;
    }

    private SerializaBLEListener serializaBLEListener;

    private SerializaBLEOutputListener serializaBLEOutputListener;
    private SerializaBLEInputListener serializaBLEInputListener;

    private BluetoothGatt bluetoothGatt;

    private BluetoothGattService service;
    private BluetoothGattCharacteristic chTX;
    private BluetoothGattDescriptor chTXNotifyDescr;
    private BluetoothGattCharacteristic chRX;
    private BluetoothGattDescriptor chRXNotifyDescr;


    public void setSerializaBLEListener(SerializaBLEListener serializaBLEListener) {
        this.serializaBLEListener = serializaBLEListener;
    }

    public void setSerializaBLEOutputListener(SerializaBLEOutputListener serializaBLEOutputListener) {
        this.serializaBLEOutputListener = serializaBLEOutputListener;
        setReadingState(false);
    }

    public void setSerializaBLEInputListener(SerializaBLEInputListener serializaBLEInputListener) {
        this.serializaBLEInputListener = serializaBLEInputListener;
    }

    private SerializaBLEClient() {
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        super.onConnectionStateChange(gatt, status, newState);
        Log.v(TAG, "onConnectionStateChange: status: " + status + " newState: " + newState);

        if (serializaBLEListener != null)
            serializaBLEListener.onConnectionStateChanged(gatt.getDevice(), newState);

        String newStateStr = "UNKNOWN";

        switch (newState) {
            case BluetoothProfile.STATE_CONNECTED:

                this.bluetoothGatt = gatt;
                gatt.discoverServices();
                newStateStr = "STATE_CONNECTED";
                break;
            case BluetoothProfile.STATE_DISCONNECTED:
                newStateStr = "STATE_DISCONNECTED";
                gatt.close();
                this.bluetoothGatt = null;

                if (serializaBLEListener != null)
                    serializaBLEListener.onChannelLost();

                break;
        }

        Log.d(TAG, "onConnectionStateChange() called with: gatt = [" + gatt.getDevice().getName() + "], status = [" + status + "], newState = [" + newStateStr + "]");

    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        super.onServicesDiscovered(gatt, status);
        Log.d(TAG, "onServicesDiscovered() called with: gatt = [" + gatt.getDevice().getName() + "], status = [" + status + "]");

        if (BluetoothGatt.GATT_SUCCESS == status) {
            for (BluetoothGattService service : gatt.getServices()) {
                Log.v(TAG, "Service: " + service.getUuid());
                if (SERVICE_UUID == null || service.getUuid().equals(SERVICE_UUID)) {

                    for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {

                        if (characteristic.getUuid().equals(CHARACTERISTIC_TX_UUID)) {
                            chTX = characteristic;
                            boolean s = gatt.setCharacteristicNotification(characteristic, true);
                            Log.i("uuidCharacteristicWrite", CHARACTERISTIC_TX_UUID.toString()
                                    + " - notificationEnabled: " + s
                            );
                        } else if (characteristic.getUuid().equals(CHARACTERISTIC_RX_UUID)) {
                            chRX = characteristic;
                            boolean s = gatt.setCharacteristicNotification(characteristic, true);
                            Log.i("uuidCharacteristicRead", CHARACTERISTIC_RX_UUID.toString()
                                    + " - notificationEnabled: " + s
                            );
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // Request the MTU size to device.
                        // See also https://stackoverflow.com/questions/24135682/android-sending-data-20-bytes-by-ble
                        boolean requestMtu = bluetoothGatt.requestMtu(512);
                        Log.v(TAG, "requestMtu: " + requestMtu);
                    }

                    return;
                }
            }
            Log.e(TAG, "Could not find uuidService:" + SERVICE_UUID.toString());
        } else {
            Log.e(TAG, "onServicesDiscovered error " + status);
        }

        // If arrived here, no service or characteristic has been found.
        gatt.disconnect();
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        super.onCharacteristicRead(gatt, characteristic, status);

        Log.d(TAG, "onCharacteristicRead() called with: gatt = [" + gatt.getDevice().getName() + "], characteristic = [" + characteristic.getUuid().toString() + "], status = [" + status + "]");

        byte[] data = characteristic.getValue();

        if (characteristic.equals(chRX) && BluetoothGatt.GATT_SUCCESS == status) {

            if (serializaBLEInputListener != null)
                serializaBLEInputListener.onDataReceived(data);

            byte res = ACK;
            characteristic.setValue(new byte[]{res});
            gatt.writeCharacteristic(characteristic);
        } else {
            System.err.println("RX error " + status);
        }
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        super.onCharacteristicWrite(gatt, characteristic, status);

        Log.d(TAG, "onCharacteristicWrite() called with: gatt = [" + gatt.getDevice().getName() + "], characteristic = [" + characteristic.getUuid().toString() + "], status = [" + status + "]");

        if (characteristic.equals(chRX)) {
            Log.w(TAG, "Stop S->C transaction: " + System.currentTimeMillis());
            createReadTimeout();
//            setReadingState(false);

        }

    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        super.onCharacteristicChanged(gatt, characteristic);

        Log.d(TAG, "onCharacteristicChanged() called with: gatt = [" + gatt.getDevice().getName() + "], characteristic = [" + characteristic.getUuid().toString() + "]");

        if (characteristic.equals(chRX)) {

            Log.w(TAG, "Start S->C transaction: " + System.currentTimeMillis());

            cancelTimeout();
            setReadingState(true);

            gatt.readCharacteristic(characteristic);

        }

        if (characteristic.equals(chTX)) {

            Log.w(TAG, "Stop C->S transaction: " + System.currentTimeMillis());

            byte res = characteristic.getValue()[0];

            if (res == ACK)
                if (serializaBLEOutputListener != null)
                    try {
                        serializaBLEOutputListener.onDataSent();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
        }
    }


    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        super.onDescriptorRead(gatt, descriptor, status);
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        super.onDescriptorWrite(gatt, descriptor, status);
    }

    @Override
    public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
        super.onReliableWriteCompleted(gatt, status);
    }

    @Override
    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        super.onReadRemoteRssi(gatt, rssi, status);
    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        super.onMtuChanged(gatt, mtu, status);

        Log.d(TAG, "onMtuChanged() called with: gatt = [" + gatt.getDevice().getName() + "], mtu = [" + mtu + "], status = [" + status + "]");

        // Receive the requested MTU size.
        // See also https://stackoverflow.com/questions/24135682/android-sending-data-20-bytes-by-ble
        if (status == BluetoothGatt.GATT_SUCCESS) {
            // It discounts 3 bytes of metadata.
            if (serializaBLEOutputListener != null)
                serializaBLEOutputListener.onMaxTrasfertSizeChanged(mtu - 3);

            if (serializaBLEListener != null)
                serializaBLEListener.onChannelReady();
        }
    }


    /**
     * Writes next packet to the Characteristic.
     */
    public void writeCharacteristic(byte[] data) throws IOException {

        Log.d(TAG, "writeCharacteristic: " + data.length);

        Log.w(TAG, "Start C->S transaction: " + System.currentTimeMillis());

        if (bluetoothGatt != null) {

            boolean setValue = chTX.setValue(data);

            boolean writeCharacteristic = bluetoothGatt.writeCharacteristic(chTX);

            if (!(setValue && writeCharacteristic))
                throw new IOException("Error during write");

        } else
            throw new IOException("No devices!");
    }

    private void setReadingState(boolean b) {
        if (serializaBLEOutputListener != null)
            serializaBLEOutputListener.onReadingStateChanged(b);
    }

    private Timer timeout;

    private void createReadTimeout() {
        cancelTimeout();

        if (timeout == null) {
            timeout = new Timer();
            timeout.schedule(new TimerTask() {
                @Override
                public void run() {
                    Log.d(TAG, "Read Timeout");
                    setReadingState(false);
                }
            }, READ_TIMEOUT);

        }
    }

    private void cancelTimeout() {
        if (timeout != null) {
            timeout.cancel();
            timeout = null;
        }
    }
}
