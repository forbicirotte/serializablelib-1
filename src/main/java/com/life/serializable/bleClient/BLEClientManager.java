package com.life.serializable.bleClient;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.life.serializable.interfaces.OnBLEScanCallback;
import com.life.serializable.stream.SerializaBLEInputStream;
import com.life.serializable.stream.SerializaBLEOutputStream;
import com.life.serializable.utils.Consts.Role;

import static com.life.serializable.utils.Consts.SERVICE_UUID;

/**
 * Created by User on 02/02/2018.
 */

public class BLEClientManager {

    private final static String TAG = "BLEClientManager";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter btAdapter;
    private BluetoothGatt bluetoothGatt;

    private OnBLEScanCallback onScanCallback;

    SerializaBLEOutputStream serializaBLEOutputStream;
    SerializaBLEInputStream serializaBLEInputStream;

    private Context context;

    public void setOnScanCallback(OnBLEScanCallback onScanCallback) {
        this.onScanCallback = onScanCallback;
    }

    public BLEClientManager(Context context) {

        this.context = context;

        serializaBLEOutputStream = new SerializaBLEOutputStream(Role.CLIENT);
        serializaBLEInputStream = new SerializaBLEInputStream(Role.CLIENT);

        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = mBluetoothManager.getAdapter();


        Log.d(TAG, "BLEServerManager: this is my address " + btAdapter.getAddress());

        if (!btAdapter.isEnabled())
            enableBt();
        else
            startBle();

        context.registerReceiver(STATE_CHANGED, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        context.registerReceiver(SCAN_MODE_CHANGED, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));

        context.registerReceiver(PAIRING_REQUEST, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST));

        context.registerReceiver(BOND_STATE_CHANGED, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));

    }

    BroadcastReceiver STATE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)) {
                case BluetoothAdapter.STATE_ON:
                    Log.d(TAG, "State Changed: STATE_ON");
                    startBle();
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d(TAG, "State Changed: STATE_TURNING_ON");
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Log.d(TAG, "State Changed: STATE_TURNING_OFF");
//                    stopBle();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    Log.d(TAG, "State Changed: STATE_OFF");
                    break;
            }
        }
    };

    BroadcastReceiver SCAN_MODE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Scan Changed");
            switch (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.SCAN_MODE_NONE)) {
                case BluetoothAdapter.SCAN_MODE_NONE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_NONE");
                    break;
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_CONNECTABLE");
                    break;
                case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                    Log.d(TAG, "ScanMode: SCAN_MODE_CONNECTABLE_DISCOVERABLE");
                    break;
            }

        }
    };

    BroadcastReceiver PAIRING_REQUEST = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            int pairingVariant = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT, BluetoothDevice.ERROR);
            int pairingKey = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_KEY, BluetoothDevice.ERROR);

            Log.d(TAG, "ACTION_PAIRING_REQUEST - " + dev.getName() + " " + dev.getAddress() + "\n" + pairingVariant + " " + pairingKey);

            String pairVar = "UNKNOWN";

            switch (pairingVariant) {
                case BluetoothDevice.PAIRING_VARIANT_PIN:
                    pairVar = "PAIRING_VARIANT_PIN";
                    break;
                case BluetoothDevice.PAIRING_VARIANT_PASSKEY_CONFIRMATION:
                    pairVar = "PAIRING_VARIANT_PASSKEY_CONFIRMATION";
                    break;
            }

            Log.d(TAG, pairVar);
        }
    };

    BroadcastReceiver BOND_STATE_CHANGED = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            String bond = "BOND_NONE";

            switch (intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE)) {
                case BluetoothDevice.BOND_NONE:
                    bond = "BOND_NONE";
                    break;
                case BluetoothDevice.BOND_BONDING:
                    bond = "BOND_BONDING";
                    break;
                case BluetoothDevice.BOND_BONDED:
                    bond = "BOND_BONDED";
                    break;
            }

            Log.d(TAG, "ACTION_BOND_STATE_CHANGED - " + bond + " " + dev.getName() + " " + dev.getAddress());
        }
    };

    public void enableBt() {
        if (btAdapter == null)
            return;

        if (!btAdapter.isEnabled()) {
            btAdapter.enable();
        }
    }

    public void disableBt() {
        if (btAdapter == null)
            return;

        if (btAdapter.isEnabled()) {
            stopBle();

            btAdapter.disable();
        }
    }

    private static int transport;

    @RequiresPermission(Manifest.permission.BLUETOOTH)
    public void connect(BluetoothDevice bluetoothDevice) {
        if (btAdapter != null && btAdapter.isEnabled()) {
            if (bluetoothGatt != null) {
                bluetoothGatt.disconnect();
            }

//            updateState(BluetoothStatus.CONNECTING);

            /*
            About this issue:
            https://code.google.com/p/android/issues/detail?id=92949
            http://stackoverflow.com/q/27633680/2826279
             */

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                transport = BluetoothDevice.TRANSPORT_LE;
                // If android verion is greather or equal to Android M (23), then call the connectGatt with TRANSPORT_LE.
                bluetoothGatt = bluetoothDevice.connectGatt(context, false, SerializaBLEClient.getInstance(), transport);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // From Android LOLLIPOP (21) the transport types exists, but them are hide for use,
                // so is needed to use relfection to get the value
                try {
                    Method connectGattMethod = bluetoothDevice.getClass().getDeclaredMethod("connectGatt", Context.class, boolean.class, BluetoothGattCallback.class, int.class);
                    connectGattMethod.setAccessible(true);
                    bluetoothGatt = (BluetoothGatt) connectGattMethod.invoke(bluetoothDevice, context, false, SerializaBLEClient.getInstance(), transport);
                } catch (Exception ex) {
                    Log.d(TAG, "Error on call BluetoothDevice.connectGatt with reflection.", ex);
                }
            }

            // If any try is fail, then call the connectGatt without transport
            if (bluetoothGatt == null) {
                bluetoothGatt = bluetoothDevice.connectGatt(context, false, SerializaBLEClient.getInstance());
            }
        }
    }

    public void startScan() {
        if (onScanCallback != null)
            onScanCallback.onStartScan();

        btAdapter.stopLeScan(mLeScanCallback);

        btAdapter.startLeScan(mLeScanCallback);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, 10000);
    }

    public void stopScan() {
        btAdapter.stopLeScan(mLeScanCallback);
        if (onScanCallback != null)
            onScanCallback.onStopScan();
    }

    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @RequiresPermission(Manifest.permission.BLUETOOTH)
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            List<UUID> uuids = new ArrayList<>();
            String record = "";
            if (scanRecord.length > 0) {
                record = new String(scanRecord);
                uuids = parseUUIDs(scanRecord);
            }

            if (onScanCallback != null && uuids.contains(SERVICE_UUID)) {
                onScanCallback.onDeviceDiscovered(device, rssi);
            }
        }
    };

    public void startBle() {
        initGattCallback();
    }

    public void stopBle() {

        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
            bluetoothGatt.close();
        }

    }

    public void initGattCallback() {
        SerializaBLEClient.getInstance().setSerializaBLEInputListener(serializaBLEInputStream);
        SerializaBLEClient.getInstance().setSerializaBLEOutputListener(serializaBLEOutputStream);
    }

    private List<UUID> parseUUIDs(final byte[] advertisedData) {
        List<UUID> uuids = new ArrayList<>();

        int offset = 0;
        while (offset < (advertisedData.length - 2)) {
            int len = advertisedData[offset++];
            if (len == 0)
                break;

            int type = advertisedData[offset++];
            switch (type) {
                case 0x02: // Partial list of 16-bit UUIDs
                case 0x03: // Complete list of 16-bit UUIDs
                    while (len > 1) {
                        int uuid16 = advertisedData[offset++];
                        uuid16 += (advertisedData[offset++] << 8);
                        len -= 2;
                        uuids.add(UUID.fromString(String.format("%08x-0000-1000-8000-00805f9b34fb", uuid16)));
                    }
                    break;
                case 0x06:// Partial list of 128-bit UUIDs
                case 0x07:// Complete list of 128-bit UUIDs
                    // Loop through the advertised 128-bit UUID's.
                    while (len >= 16) {
                        try {
                            // Wrap the advertised bits and order them.
                            ByteBuffer buffer = ByteBuffer.wrap(advertisedData,
                                    offset++, 16).order(ByteOrder.LITTLE_ENDIAN);
                            long mostSignificantBit = buffer.getLong();
                            long leastSignificantBit = buffer.getLong();
                            uuids.add(new UUID(leastSignificantBit,
                                    mostSignificantBit));
                        } catch (IndexOutOfBoundsException e) {
                            // Defensive programming.
                            Log.e(TAG, e.toString());
                            continue;
                        } finally {
                            // Move the offset to read the next uuid.
                            offset += 15;
                            len -= 16;
                        }
                    }
                    break;
                default:
                    offset += (len - 1);
                    break;
            }
        }

        return uuids;
    }

    public void unregisterReceivers() {
        context.unregisterReceiver(STATE_CHANGED);
        context.unregisterReceiver(PAIRING_REQUEST);
        context.unregisterReceiver(BOND_STATE_CHANGED);
        context.unregisterReceiver(SCAN_MODE_CHANGED);
    }

    public SerializaBLEInputStream getSerializaBLEInputStream() {
        return serializaBLEInputStream;
    }

    public SerializaBLEOutputStream getSerializaBLEOutputStream() {
        return serializaBLEOutputStream;
    }
}
